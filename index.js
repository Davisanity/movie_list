// API URL
const BASE_URL = 'https://movie-list.alphacamp.io'
const INDEX_URL = BASE_URL + '/api/v1/movies/'
const POSTER_URL = BASE_URL + '/posters/'

// DOM element
const dataPanel = document.querySelector('#data-panel')
const searchForm = document.querySelector('#search-form')
const searchInput = document.querySelector('#search-input')
const paginator = document.querySelector('#paginator')
const changeDisplayMode = document.querySelector('.change-mode')

// page, movie and displaymode data
const movies = [] //電影總清單
let filteredMovies = [] //搜尋清單
const MOVIES_PER_PAGE = 12
let currentPage = 1
let cardMode = true

//BEGIN: use list API to get movie list
axios.get(INDEX_URL)
  .then((response) => {
    for (const movie of response.data.results) {
      movies.push(movie)
    }
    //console.log(movies)
    renderPaginator(movies.length)
    renderMovieList(getMoviesByPage(1))
  }).catch((err) => console.log(err))
// Approch2: use spread operator
// axios.get(INDEX_URL).then((response) => {
//   movies.push(...response.data.results)
//   console.log(movies)
// }).catch((err) => console.log(err))
//END: use list API to get movie list

//Begin:pagination //////
// pagination render function
function renderPaginator(amount) {
  const numberOfPages = Math.ceil(amount / MOVIES_PER_PAGE)
  let rawHTML = ''

  for (let page = 1; page <= numberOfPages; page++) {
    rawHTML += `<li class="page-item"><a class="page-link" href="#" data-page="${page}">${page}</a></li>`
  }
  paginator.innerHTML = rawHTML
}

function renderMovieList(data) {
  let rawHTML = ''
  if (cardMode) {
    data.forEach((item) => {
      // fill the image and title in card mode
      rawHTML += `<div class="col-sm-3">
        <div class="mb-2">
          <div class="card">
            <img src="${POSTER_URL + item.image}" class="card-img-top" alt="Movie poster">
            <div class="card-body">
              <h5 class="card-title">${item.title}</h5>
            </div>
            <div class="card-footer">
              <button class="btn btn-primary btn-show-movie" data-toggle="modal"
                data-target="#movie-modal" data-id="${item.id}">More</button>
              <button class="btn btn-info btn-add-favorite" data-id="${item.id}">+</button>
            </div>
          </div>
        </div>
      </div>`
    })

  } else {
    rawHTML = `<ul class="list-group render-movie-list container-fluid">`
    data.forEach((item) => {
      rawHTML += `<li class="list-group-item d-flex justify-content-between">${item.title}
        <div class="list-footer">
              <button class="btn btn-primary btn-show-movie" data-toggle="modal"
                data-target="#movie-modal" data-id="${item.id}">More</button>
              <button class="btn btn-info btn-add-favorite" data-id="${item.id}">+</button>
            </div>
      </li>`
    })
    rawHTML += `</ul>`
  }
  dataPanel.innerHTML = rawHTML
}

function getMoviesByPage(page) {
  const data = filteredMovies.length ? filteredMovies : movies
  const startIndex = (page - 1) * MOVIES_PER_PAGE

  return data.slice(startIndex, startIndex + MOVIES_PER_PAGE)
}
//End:pagination //////


//////  movie modal  //////

// function for rendering movie modal by id
function showMovieModal(id) {
  // get elements
  const modalTitle = document.querySelector('#movie-modal-title')
  const modalImage = document.querySelector('#movie-modal-image')
  const modalDate = document.querySelector('#movie-modal-date')
  const modalDescription = document.querySelector('#movie-modal-description')

  // send request to show api
  axios.get(INDEX_URL + id).then((response) => {
    const data = response.data.results

    // insert data into modal ui
    modalTitle.innerText = data.title
    modalDate.innerText = 'Release date: ' + data.release_date
    modalDescription.innerText = data.description
    modalImage.innerHTML = `<img src="${POSTER_URL + data.image
      }" alt="movie-poster" class="img-fluid">`
  })
}

function addToFavorite(id) {
  const list = JSON.parse(localStorage.getItem('favoriteMovies')) || []
  const movie = movies.find((movie) => movie.id === id)

  if (list.some((movie) => movie.id === id)) {
    return alert('此電影已經在收藏清單中！')
  }

  list.push(movie)
  localStorage.setItem('favoriteMovies', JSON.stringify(list))
}

////// search movies //////

function searchMovies() {
  const keyword = searchInput.value.trim().toLowerCase()
  if (keyword.length === 0) {
    searchInput.classList.add('alert')
    searchInput.value = ''
    searchInput.placeholder = '請輸入有效內容！'
  } else {
    searchInput.classList.remove('alert')
    searchInput.placeholder = 'Movie title'
  }

  filteredMovies = allMovies.filter(movie =>
    movie.title.toLowerCase().includes(keyword)
  )
  // check the matched data
  if (!filteredMovies.length) {
    return alert(`您輸入的關鍵字 ${keyword} 無法找到相關資訊`)
  } else {
    renderPaginator(filteredMovies.length)
    renderMovieList(getMoviesByPage(1))
  }
}

// 監聽 paginator
paginator.addEventListener('click', function onPaginatorClicked(event) {
  if (event.target.tagName !== 'A') return
  currentPage = Number(event.target.dataset.page)
  renderMovieList(getMoviesByPage(currentPage))
})

// add event listener to change mode
changeDisplayMode.addEventListener('click', function onChangeModeClicked(event) {
  if (event.target.matches('.list')) {
    cardMode = false
    renderMovieList(getMoviesByPage(currentPage))
  } else if (event.target.matches('.grid')) {
    cardMode = true
    renderMovieList(getMoviesByPage(currentPage))
  }
})

// 監聽 data panel
dataPanel.addEventListener('click', function onPanelClick(event) {
  // lock the event target to the "more" button
  if (event.target.matches('.btn-show-movie')) {
    // call function by return the id-data
    showMovieModal(Number(event.target.dataset.id))
    console.log("SHOW,")
  } else if (event.target.matches('.btn-add-favorite')) {
    addToFavorite(Number(event.target.dataset.id))
  }
})

//監聽表單提交事件
searchForm.addEventListener('submit', function onSearchFormSubmitted(event) {
  //取消預設事件
  event.preventDefault()
  //取得搜尋關鍵字
  const keyword = searchInput.value.trim().toLowerCase()
  //錯誤處理：輸入無效字串
  // if (!keyword.length) {
  //   return alert('請輸入有效字串！')
  // }
  //條件篩選
  filteredMovies = movies.filter((movie) =>
    movie.title.toLowerCase().includes(keyword)
  )
  if (filteredMovies.length === 0) {
    return alert(`您輸入的關鍵字：${keyword} 沒有符合條件的電影`)
  }
  //重新輸出至畫面
  renderMovieList(filteredMovies)
})
